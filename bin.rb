#!/usr/bin/env ruby

require 'fileutils'
require 'json'
include FileUtils

Dir.chdir __dir__

def bork message
    puts 'error: ' + message
    exit 1
end

def write file, text
    File.write file, text, mode: 'ab'
end

bork 'missing param' unless ARGV.size == 1

dir = ARGV[0]

bork 'not directory' unless File.directory? dir

files = Dir[dir + '/**/*'].sort.filter do |it|
    File.file? it
end

offset = 0
table  = {}

files.each do |it|
    size = File.size(it)
    table[it.delete_prefix(dir)] = { offset: offset, size: size }
    offset += size
end

table = JSON.generate(table).force_encoding(Encoding::ASCII)

file = dir + '.bin'
size = [table.size].pack('I!<')

rm_f  file
write file, size
write file, table

files.each do |it|
    write file, File.open(it).read
end
