# snek

This is a version of snake in JS, and I've built a little canvas games engine to do it (nothing fancy.)

The games engine is a bit quirky because it uses JS function scoping and callbacks to hide its private
parts, but exposes public api functions and objects through an api object.

It also expects a game to be defined as a function, into which the engine will call and pass an initialiser function. This initialiser function gives the game a chance to setup the engine and get a handle to the api. The game function is also used because it acts an main entry point, and also as a state and scoping mechanism.

When the game function has completed declaring different screen states and models, the game function then asks the engine to run, passing an initial game state to the engine. A game state is just an object with optional methods: `init`, `tick`, `draw`. There is a `next` api function that allows the game to set the next state.

# tools

I've used [PikoPixel](http://twilightedge.com/mac/pikopixel/) to draw the pixel graphics, and [Hex Fiend](http://ridiculousfish.com/hexfiend/) for viewing binary files.

# running inside an app

I've used ES modules to break up the code, but this meant I needed to work around CORS issues when loading web pages using `file://` urls on iOS. The first attempt (which worked) was to embed a [swift web server](https://github.com/httpswift/swifter) then changing ATS configuration settings to access `http` resources, and then pointing a web view at `localhost`. The second attempt (which seems to work, so I've gone with it) is to define a custom URL scheme called `local://` and to handle the local loading with a scheme handler. As resources are declared using relative paths, it looks like the scheme is applied to all resources, including JS, CSS, font, image and ES module loading too, which was a nice surprise. It looks like this bypasses the ES module CORS requirement.

# binary archive

Just for fun, the app reads resources from a binary archive. The binary archive is created when the app builds, by running a [ruby script](bin.rb) that is pointed at the [www](www) directory.

The archive format consists of a table of entries concatenated with the contents of all the files inside the [www](www) directory. The table is encoded as a json string. The table keys are relative path names, and the table values are byte ranges within the blob of data. The json is encoded in the ASCII character set, and the length of the json string is prefixed on the front as a little-endian 32-bit integer:

```
json size : int32 (little-endian)
json      : json string (ascii)
blob      : binary data
```

The json looks like the following, with a byte offset and a size:

```json
{
    "/index.html"  : { "offset": 0,   "size": 500 },
    "/foo/bar.css" : { "offset": 500, "size": 200 }
}
```

The app uses this table to return smaller blobs of data from the overall blob.

# safe area inside an app

I've used native safe area constraints to position the web view; I investigated using `env()` and safe area insets but this was over-complicating the already complicated CSS layout viewport calculations, and caused a glitch when the app was launched in landscape (which righted itself when rotating to portrait and back.)

# testing locally

This project uses ES modules so you'll need to host these locally to get around CORS if you want to view the web page in a desktop browser. If you've got ruby installed, you can do this one-liner:

```
ruby -run -e httpd ./www -p 8080
```

# references

- [Modules in browsers](https://jakearchibald.com/2017/es-modules-in-browsers/)
- [JavaScript modules](https://v8.dev/features/modules)
- [Designing websites for iPhone X](https://webkit.org/blog/7929/designing-websites-for-iphone-x/)
- [Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
- [CSS variables](https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties)
- [Better Xcode Run Script Build Phases](https://www.mokacoding.com/blog/better-build-phase-scripts/)
- [What the pack?](https://idiosyncratic-ruby.com/4-what-the-pack.html)
- [US-ASCII-8BIT](https://idiosyncratic-ruby.com/56-us-ascii-8bit.html)
- [Destructuring assignment](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment)
- [Pointer events](https://developer.mozilla.org/en-US/docs/Web/API/Pointer_events)
- [Using CSS gradients](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Images/Using_CSS_gradients)
