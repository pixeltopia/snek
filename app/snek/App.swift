import UIKit
import WebKit

@UIApplicationMain
class App: UIResponder, UIApplicationDelegate {

    typealias LaunchOptions = [UIApplication.LaunchOptionsKey: Any]

    var window: UIWindow?

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: LaunchOptions?
    ) -> Bool {
        true
    }
}

class ViewController: UIViewController, MessagingDelegate {

    var webView: WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        let handler = SchemeHandler()

        do {
            try handler.load("www.bin")
        } catch {
            return print(error)
        }

        let configuration = WKWebViewConfiguration()
        configuration.setURLSchemeHandler(handler, forURLScheme: "local")
        configuration.userContentController.add(Messaging(self), name: "bridge")

        webView = WKWebView(frame: view.bounds, configuration: configuration)
        webView.isOpaque = true
        view.addSubview(webView)

        if let view = view {
            constrain(webView, view)
        }

        if let url = URL(string: "local:///index.html") {
            webView.load(URLRequest(url: url))
        }
    }

    func constrain(_ one: UIView, _ two: UIView) {
        one.translatesAutoresizingMaskIntoConstraints = false
        two.translatesAutoresizingMaskIntoConstraints = false

        let constraints = [
            one.topAnchor.constraint(equalTo: two.safeAreaLayoutGuide.topAnchor),
            one.bottomAnchor.constraint(equalTo: two.safeAreaLayoutGuide.bottomAnchor),
            one.leftAnchor.constraint(equalTo: two.safeAreaLayoutGuide.leftAnchor),
            one.rightAnchor.constraint(equalTo: two.safeAreaLayoutGuide.rightAnchor),
        ]

        NSLayoutConstraint.activate(constraints)

        two.addConstraints(constraints)
    }

    func rumble(_ style: UIImpactFeedbackGenerator.FeedbackStyle) {
        UIImpactFeedbackGenerator(style: style).impactOccurred()
    }
}

class SchemeHandler: NSObject, WKURLSchemeHandler {

    enum Error: Swift.Error {
        case error
    }

    struct Entry: Decodable {
        let offset: Int
        let size:   Int
    }

    typealias Table = [String: Entry]
    typealias Stuff = (Table, Data)

    var stuff: Stuff?

    func load(_ file: String) throws {
        let url       = Bundle.main.bundleURL.appendingPathComponent(file)
        let data      = try Data(contentsOf: url, options: .mappedIfSafe)
        let jsonSize  = Int(data[0]) | Int(data[1]) << 8 | Int(data[2]) << 16 | Int(data[3]) << 24
        let jsonStart = 4
        let blobStart = jsonStart + jsonSize
        let json      = data.subdata(in: jsonStart..<blobStart)
        let blob      = data.subdata(in: blobStart..<data.count)
        let table     = try JSONDecoder().decode(Table.self, from: json)

        stuff = (table, blob)
    }

    func webView(_ webView: WKWebView, start urlSchemeTask: WKURLSchemeTask) {
        guard
            let url           = urlSchemeTask.request.url,
            let (table, blob) = stuff,
            let entry         = table[url.path]
            else { return urlSchemeTask.didFailWithError(Error.error) }

        urlSchemeTask.didReceive(
            URLResponse(
                url: url,
                mimeType: contentType(url),
                expectedContentLength: entry.size,
                textEncodingName: nil
            )
        )
        urlSchemeTask.didReceive(
            blob.subdata(in: entry.offset..<entry.offset + entry.size)
        )
        urlSchemeTask.didFinish()
    }

    func contentType(_ url: URL) -> String {
        switch url.pathExtension {
        case "html": return "text/html"
        case "css":  return "text/css"
        case "js":   return "application/javascript"
        case "png":  return "image/png"
        default:     return "application/octet-stream"
        }
    }

    func webView(_ webView: WKWebView, stop urlSchemeTask: WKURLSchemeTask) {
    }
}

class Messaging: NSObject, WKScriptMessageHandler {

    weak var delegate: MessagingDelegate?

    init(_ delegate: MessagingDelegate) {
        self.delegate = delegate
    }

    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        guard let payload = message.body as? [String: AnyObject] else { return }
        if payload["name"] as? String == "rumble", let style = payload["style"] as? String {
            switch style {
            case "light": delegate?.rumble(.light)
            case "heavy": delegate?.rumble(.heavy)
            default: break
            }
        }
    }
}

protocol MessagingDelegate: class {
    func rumble(_ style: UIImpactFeedbackGenerator.FeedbackStyle)
}
