const portrait = window.matchMedia('(orientation: portrait)');

const listener = () => {
    const containerOne = document.getElementById('containerOne');
    const containerTwo = document.getElementById('containerTwo');
    const joypad = document.getElementById('joypad');
    const button = document.getElementById('button');

    const clear = (element) => {
        while (element.firstChild) {
            element.removeChild(element.firstChild);
        }
    };

    clear(containerOne);
    clear(containerTwo);

    if (portrait.matches) {
        const stretch = document.createElement('div');
        stretch.id = 'stretch';
        containerTwo.append(joypad);
        containerTwo.append(stretch);
        containerTwo.append(button);
    } else {
        containerOne.append(joypad);
        containerTwo.append(button);
    }
};

portrait.addListener(listener);

listener();
