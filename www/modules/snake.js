export const snake = (init) => {

    const TILE_W   = 8;
    const TILE_H   = 8;
    const GRID_W   = 11;
    const GRID_H   = 11;
    const SCREEN_W = TILE_W * GRID_W;
    const SCREEN_H = TILE_H * GRID_H;

    const BACK1 = 0;
    const BACK2 = 1;
    const SNAKE = 2;
    const BONCE = 3;
    const FRUIT = 4;

    const DIRECTION_UP = 1;
    const DIRECTION_DN = 2;
    const DIRECTION_LF = 3;
    const DIRECTION_RT = 4;

    let ticked;
    let snake;
    let fruit;
    let score;

    const {
        util: { ticker, rumble, random, memcpy, alloc, realloc },
        geom: { point, size, rect },
        next,
        run,
        ctl,
        gfx,
    } = init(SCREEN_W, SCREEN_H);

    class Thing {
        constructor(x = 0, y = 0, direction = 0) {
            this.set(x, y, direction);
        }
        set(x, y, direction) {
            this.x = x;
            this.y = y;
            if (direction !== undefined) {
                this.direction = direction;
            }
        }
        move() {
            switch (this.direction) {
                case DIRECTION_UP: this.y--; break;
                case DIRECTION_DN: this.y++; break;
                case DIRECTION_LF: this.x--; break;
                case DIRECTION_RT: this.x++; break;
            }
            this.wrap();
        }
        wrap() {
            this.set(
                (this.x < 0 ? GRID_W + this.x : this.x) % GRID_W,
                (this.y < 0 ? GRID_H + this.y : this.y) % GRID_H,
            )
        }
    }

    class Snake {
        constructor(head, size, capacity) {
            this.head      = head;
            this.size      = size;
            this.capacity  = capacity;

            this.x = alloc(Int8Array, capacity);
            this.y = alloc(Int8Array, capacity);

            for (let i = 0; i < size; i++) {
                this.x[i] = head.x;
                this.y[i] = head.y;
            }
        }
        face(direction) {
            if (direction && direction !== opposite(this.head.direction)) {
                this.head.direction = direction;
            }
        }
        move() {
            memcpy(this.x, 0, this.x, 1, this.size - 1);
            memcpy(this.y, 0, this.y, 1, this.size - 1);

            this.head.set(this.x[0], this.y[0]);
            this.head.move();

            this.x[0] = this.head.x;
            this.y[0] = this.head.y;
        }
        grow(increment) {
            const last = this.size - 1;
            const x    = this.x[last];
            const y    = this.y[last];

            this.size += increment;

            if (this.size > this.capacity) {
                this.capacity += increment * 4;
                this.x = realloc(this.x, this.capacity);
                this.y = realloc(this.y, this.capacity);
            }

            for (let i = last + 1; i < this.size; i++) {
                this.x[i] = x;
                this.y[i] = y;
            }
        }
        eats(it) {
            return (this.x[0] === it.x && this.y[0] === it.y) ||
                   (this.x[1] === it.x && this.y[1] === it.y);
        }
        dies() {
            return this.includes(this.x[0], this.y[0], 1);
        }
        includes(x, y, from = 0) {
            for (let i = from; i < this.size; i++) {
                if (this.x[i] === x && this.y[i] === y) {
                    return true;
                }
            }
            return false;
        }
        bonce(x, y) {
            return this.x[0] === x && this.y[0] === y;
        }
    }

    class Fruit extends Thing {
        constructor() {
            super();
            this.spawn();
        }
        spawn() {
            this.set(
                random(0, GRID_W - 1),
                random(0, GRID_H - 1),
                random(DIRECTION_UP, DIRECTION_RT)
            );
        }
        includes(x, y) {
            return this.x === x && this.y === y;
        }
    }

    const direction = () => {
        if (ctl.up) return DIRECTION_UP;
        if (ctl.dn) return DIRECTION_DN;
        if (ctl.lf) return DIRECTION_LF;
        if (ctl.rt) return DIRECTION_RT;
        return 0;
    };

    const opposite = (direction) => {
        switch (direction) {
            case DIRECTION_UP: return DIRECTION_DN;
            case DIRECTION_DN: return DIRECTION_UP;
            case DIRECTION_LF: return DIRECTION_RT;
            case DIRECTION_RT: return DIRECTION_LF;
            default: return 0;
        }
    };

    const draw = () => {
        let   floor  = BACK1;
        const cursor = point(0, 0);

        for (let y = 0; y < GRID_H; y++) {
            cursor.x = 0;
            for (let x = 0; x < GRID_W; x++) {
                gfx.tile(floor, cursor);

                if (snake.bonce(x, y)) {
                    gfx.tile(BONCE, cursor);
                } else if (snake.includes(x, y)) {
                    gfx.tile(SNAKE, cursor);
                }

                if (fruit.includes(x, y)) {
                    gfx.tile(FRUIT, cursor);
                }

                floor = floor === BACK1 ? BACK2 : BACK1;
                cursor.x += TILE_W;
            }
            cursor.y += TILE_H;
        }

        gfx.font(0, 8);
        const { width } = gfx.measure(score);
        gfx.rect(rect(point(0, 0), size(width, 8)), '#000000');
        gfx.text(score, point(0, 7));
    };

    // states

    const load = {
        init: async () => {
            try {
                await gfx.setup('tiles.png', size(TILE_W, TILE_H));
                await gfx.loadFonts();
                gfx.anim(FRUIT, [4, 5, 6, 7], 125);
                next(play);
            } catch (err) {
                console.error(err);
                next(fail);
            }
        }
    };

    const fail = {
        draw: () => gfx.clear('red')
    };

    const play = {
        init: () => {
            ticked = ticker(250);
            snake  = new Snake(new Thing(GRID_W / 2, GRID_H / 2, DIRECTION_UP), 3, 10);
            fruit  = new Fruit();
            score  = 0;
        },
        tick: () => {
            snake.face(direction());

            if (! ticked()) {
                return;
            }

            snake.move();
            fruit.move();

            if (snake.eats(fruit)) {
                snake.grow(3);
                fruit.spawn();
                score++;
                rumble('light', 100, 2);
            }

            if (snake.dies()) {
                next(died);
            }
        },
        draw: () => draw(),
    };

    const died = {
        init: () => {
            rumble('light', 10, 10);
            rumble('blank', 100, 1);
            rumble('light', 10, 10);
            rumble('blank', 100, 1);
            rumble('light', 10, 10);
        },
        tick: () => {
            if (ctl.sp) {
                next(play);
            }
        },
        draw: () => {
            draw();

            const text   = 'GAME OVER';
            const height = 8;

            gfx.font(0, height, 'center', 'middle');
            gfx.rect(rect(point(0, gfx.middle.y - height / 2), size(SCREEN_W, height)), '#000000');
            gfx.text(text, gfx.middle);
        }
    };

    run(load);

};
