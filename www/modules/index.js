import './layout.js';

import { snake } from './snake.js';
import { lib }   from './lib.js';

const { engine, onscreen, device } = lib;

engine(snake, 'canvas', onscreen('joypad', 'button', device), 'font');
