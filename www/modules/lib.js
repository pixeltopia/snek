import * as geom from './lib/geom.js';
import * as util from './lib/util.js';

import { Surface, Graphics } from './lib/gfx.js';
import { onscreen }          from './lib/ctl.js';
import { device, Rumbler }   from './lib/device.js';

export const lib = (() => {

    const engine = (game, id, ctl, ...fonts) => {
        game((width, height, step = 1000 / 60) => {
            const size = { width, height };
            const gfx  = new Graphics(new Surface({ size }), fonts);
            const scr  = new Graphics(new Surface({ id }));

            const initState = (state, params) => {
                if (state.init) {
                    if (params) {
                        state.init(...params);
                    } else {
                        state.init();
                    }
                }
            };

            let pending;

            const next = (state, ...params) => {
                pending = { state, params };
            };

            const ticker = (interval) => {
                let value = 0;
                return () => {
                    if ((value = value + step) < interval) {
                        return false;
                    }
                    value = 0;
                    return true;
                };
            };

            let rumblers = [];

            const rumble = (style, ticks = 0, times = 1) => {
                rumblers.push(new Rumbler(device, ticker, style, ticks, times));
            };

            const run = (state) => {

                let current = state;

                let last        = undefined;
                let accumulated = 0;
                let delta       = 0;
                let ticked      = false;

                const frame = (now) => {
                    if (last === undefined) {
                        last = now;
                    }

                    delta = now - last;
                    last  = now;

                    accumulated += delta;

                    ticked = false;

                    // fixed timestep ticking
                    while (accumulated >= step) {
                        // rumblers
                        if (rumblers.length) {
                            rumblers[0].tick();
                        }
                        // animations
                        gfx.tick(step);
                        // state
                        if (current.tick) {
                            current.tick();
                        }
                        accumulated -= step;
                        ticked = true;
                    }

                    if (ticked) {
                        if (current.draw) {
                            current.draw();
                        }
                        scr.blit(gfx);
                    }

                    // state transition
                    if (pending) {
                        const { state, params } = pending;
                        current = state;
                        pending = null;
                        initState(state, params);
                    }

                    if (rumblers.length && rumblers[0].done) {
                        rumblers.shift();
                    }

                    window.requestAnimationFrame(frame);
                };

                initState(current);

                window.requestAnimationFrame(frame);
            };

            return Object.freeze({
                util: { ticker, rumble, ...util },
                geom,
                next,
                run,
                ctl,
                gfx,
            });
        });
    };

    return { engine, onscreen, device };

})();
