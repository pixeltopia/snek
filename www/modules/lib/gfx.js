import * as geom  from './geom.js';
import { consts } from './util.js';

export class Surface {
    constructor({ id, size, smoothing = false }) {
        const canvas = id ? document.getElementById(id) : document.createElement('canvas');
        const { width, height } = size || canvas.getBoundingClientRect();

        const context = canvas.getContext('2d');
        const ratio   = (window.devicePixelRatio || 1) / (context.backingStorePixelRatio || 1);
        canvas.width  = ratio * width;
        canvas.height = ratio * height;

        context.setTransform(ratio, 0, 0, ratio, 0, 0);
        context.imageSmoothingEnabled = smoothing;

        this.canvas  = canvas;
        this.context = context;
        this.size    = geom.size(width, height);
    }
    normalised({ clientX, clientY }) {
        const { x, y, width, height } = this.canvas.getBoundingClientRect();
        return geom.point((clientX - x) / width, (clientY - y) / height);
    }
}

export class Graphics {
    constructor(surface, fonts) {
        this.surface    = surface;
        this.animations = {};
        this.fonts      = fonts;
    }
    get width() {
        return this.surface.size.width;
    }
    get height() {
        return this.surface.size.height;
    }
    get middle() {
        return geom.point(
            this.surface.size.width / 2,
            this.surface.size.height / 2
        );
    }
    get context() {
        return this.surface.context;
    }
    async setup(url, size) {
        this.sheet = new Sheet(await load(url), size);
    }
    async loadFonts() {
        if (this.fonts) {
            return Promise.all(this.fonts.map((it) => document.fonts.load(`1em ${it}`)));
        } else {
            return Promise.resolve();
        }
    }
    clear(colour) {
        if (colour) {
            this.context.fillStyle = colour;
            this.context.fillRect(0, 0, this.width, this.height);
        } else {
            this.context.clearRect(0, 0, this.width, this.height);
        }
    }
    blit({ surface: { canvas } }) {
        this.context.drawImage(canvas, 0, 0, this.width, this.height);
    }
    tile(index, position, align = consts.ALIGN_LEFT | consts.ALIGN_TOP) {
        const anim = this.animations[index];
        if (anim) {
            index = anim.frame;
        }
        this.sheet.draw(this.context, index, position, align);
    }
    anim(index, frames, step) {
        this.animations[index] = new Animation(frames, step);
    }
    font(index, size, align = 'left', baseline = 'alphabetic') {
        const font = this.fonts[index] || '';
        this.context.font         = `${size}px ${font}`;
        this.context.textAlign    = align;
        this.context.textBaseline = baseline;
    }
    text(string, point, colour = '#FFFFFF') {
        this.context.fillStyle = colour;
        this.context.fillText(string, point.x, point.y);
    }
    rect(rect, colour = '#000000') {
        this.context.fillStyle = colour;
        this.context.fillRect(rect.x, rect.y, rect.width, rect.height);
    }
    measure(string) {
        return this.context.measureText(string);
    }
    tick(step) {
        for (const next in this.animations) {
            this.animations[next].tick(step);
        }
    }
}

const load = async (url) =>
    new Promise((resolve, reject) => {
        const image   = new Image();
        image.onload  = () => resolve(image);
        image.onerror = reject;
        image.src     = url;
    });

class Sheet {
    constructor(image, size) {
        this.image       = image;
        this.size        = size;
        this.tilesAcross = Math.floor(this.image.width / this.size.width);
    }
    draw(context, index, position, align = consts.ALIGN_LEFT | consts.ALIGN_TOP) {
        const { x, y, size = this.size } = position; // position := point | rect
        const draw = geom.rect(geom.point(x, y), size);
        const tile = geom.rect(
            geom.point(
                Math.floor(index % this.tilesAcross) * this.size.width,
                Math.floor(index / this.tilesAcross) * this.size.height,
            ),
            this.size,
        );
        const offset = geom.point(0, 0);

        if (align & consts.ALIGN_CENTRE) {
            offset.x = -draw.width / 2;
        } else if (align & consts.ALIGN_RIGHT) {
            offset.x = -draw.width;
        }

        if (align & consts.ALIGN_MIDDLE) {
            offset.y = -draw.height / 2;
        } else if (align & consts.ALIGN_BOTTOM) {
            offset.y = -draw.height;
        }

        draw.add(offset);

        context.drawImage(
            this.image,
            tile.x, tile.y, tile.width, tile.height,
            draw.x, draw.y, draw.width, draw.height,
        );
    }
}

class Animation {
    constructor(frames, step) {
        this.index  = 0;
        this.frames = frames;
        this.step   = step;
        this.count  = step;
    }
    get frame() {
        return this.frames[this.index];
    }
    tick(step) {
        this.count -= step;
        if (this.count <= 0) {
            this.index = (this.index + 1) % this.frames.length;
            this.count = this.step;
        }
    }
}
