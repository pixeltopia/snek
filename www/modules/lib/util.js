export const consts = Object.freeze({
    ALIGN_LEFT:   1 << 0,
    ALIGN_CENTRE: 1 << 1,
    ALIGN_RIGHT:  1 << 2,

    ALIGN_TOP:    1 << 3,
    ALIGN_MIDDLE: 1 << 4,
    ALIGN_BOTTOM: 1 << 5,
});

export const random = (min, max) =>
    Math.floor(min + Math.random() * (max - min));

export const memcpy = (src, srcOffset, dst, dstOffset, length) =>
    dst.set(src.subarray(srcOffset, srcOffset + length), dstOffset);

export const alloc = (constructor, length) =>
    new constructor(new ArrayBuffer(length * constructor.BYTES_PER_ELEMENT));

export const realloc = (src, length) => {
    const { buffer, constructor } = src;

    const byteLength = length * constructor.BYTES_PER_ELEMENT;
    if (byteLength < buffer.byteLength) {
        return new constructor(buffer.slice(0, byteLength));
    }

    const result = new constructor(new ArrayBuffer(byteLength));
    result.set(src);
    return result;
};
