export const point = (x, y) =>
    new Point(x, y);

export const size = (width, height) =>
    new Size(width, height);

export const rect = (origin, size) =>
    new Rect(origin, size);

class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    add({ x, y }) {
        this.x += x;
        this.y += y;
    }
    scaled(sx, sy) {
        return point(this.x * sx, this.y * sy);
    }
}

class Size {
    constructor(width, height) {
        this.width  = width;
        this.height = height;
    }
    scaled(sx, sy) {
        return size(this.width * sx, this.height * sy);
    }
}

class Rect {
    constructor(origin, size) {
        this.origin = origin;
        this.size   = size;
    }
    get x() {
        return this.origin.x;
    }
    get y() {
        return this.origin.y;
    }
    get width() {
        return this.size.width;
    }
    get height() {
        return this.size.height;
    }
    get maxx() {
        return this.origin.x + this.width;
    }
    get maxy() {
        return this.origin.y + this.height;
    }
    add(offset) {
        this.origin.add(offset);
    }
    contains(point) {
        return this.x <= point.x && point.x <= this.maxx &&
               this.y <= point.y && point.y <= this.maxy;
    }
    scaled(sx, sy) {
        return rect(this.origin.scaled(sx, sy), this.size.scaled(sx, sy));
    }
}
