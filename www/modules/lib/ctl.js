import * as geom             from './geom.js';
import { Surface, Graphics } from './gfx.js';

class Controller {
    constructor() {
        this.resetDirections();
        this.resetActions();
    }
    resetDirections() {
        this.up = false;
        this.dn = false;
        this.lf = false;
        this.rt = false;
    }
    resetActions() {
        this.sp = false;
    }
}

class Combined {
    constructor(one, two) {
        this.one = one;
        this.two = two;
    }
    get up() { return this.one.up || this.two.up; }
    get dn() { return this.one.dn || this.two.dn; }
    get lf() { return this.one.lf || this.two.lf; }
    get rt() { return this.one.rt || this.two.rt; }
    get sp() { return this.one.sp || this.two.sp; }
}

const joypad = async (controller, id, device) => {
    const surface = new Surface({ id, smoothing: true });
    const gfx = new Graphics(surface);

    await gfx.setup('buttons.png', geom.size(100, 100));

    const size  = geom.size(1 / 3, 1 / 3);
    const inset = 1 / 16;

    const up = geom.rect(geom.point(size.width, inset), size);
    const dn = geom.rect(geom.point(size.width, 1 - size.height - inset), size);
    const lf = geom.rect(geom.point(inset, size.height), size);
    const rt = geom.rect(geom.point(1 - size.width - inset, size.height), size);

    const handler = (event) => {
        const point = surface.normalised(event);

        const oldUp = controller.up;
        const oldDn = controller.dn;
        const oldLf = controller.lf;
        const oldRt = controller.rt;

        controller.up = up.contains(point);
        controller.dn = dn.contains(point);
        controller.lf = lf.contains(point);
        controller.rt = rt.contains(point);

        let changed =
            (controller.up && !oldUp) ||
            (controller.dn && !oldDn) ||
            (controller.lf && !oldLf) ||
            (controller.rt && !oldRt);

        if (changed) {
            device.rumble('light');
        }
    };

    surface.canvas.addEventListener('pointerdown', handler);
    surface.canvas.addEventListener('pointermove', handler);
    surface.canvas.addEventListener('pointerleave', () => {
        controller.resetDirections();
    });

    const paint = () => {
        gfx.clear();

        if (controller.up) {
            gfx.tile(1, up.scaled(gfx.width, gfx.height));
        } else {
            gfx.tile(6, up.scaled(gfx.width, gfx.height));
        }

        if (controller.lf) {
            gfx.tile(0, lf.scaled(gfx.width, gfx.height));
        } else {
            gfx.tile(5, lf.scaled(gfx.width, gfx.height));
        }

        if (controller.dn) {
            gfx.tile(3, dn.scaled(gfx.width, gfx.height));
        } else {
            gfx.tile(8, dn.scaled(gfx.width, gfx.height));
        }

        if (controller.rt) {
            gfx.tile(2, rt.scaled(gfx.width, gfx.height));
        } else {
            gfx.tile(7, rt.scaled(gfx.width, gfx.height));
        }

        window.requestAnimationFrame(paint);
    };

    paint();
};

const button = async (controller, id, device) => {
    const surface = new Surface({ id, smoothing: true });
    const gfx = new Graphics(surface);

    await gfx.setup('buttons.png', geom.size(100, 100));

    const size = geom.size(1 / 3, 1 / 3);

    const sp = geom.rect(
        geom.point(1 / 2 - (size.width / 2), 1 / 2 - (size.height / 2)),
        size
    );

    const handler = (event) => {
        const oldSp = controller.sp;

        controller.sp = sp.contains(surface.normalised(event));

        let changed = (controller.sp && !oldSp);
        if (changed) {
            device.rumble('light');
        }
    };

    surface.canvas.addEventListener('pointerdown', handler);
    surface.canvas.addEventListener('pointermove', handler);
    surface.canvas.addEventListener('pointerleave', () => {
        controller.resetActions();
    });

    const paint = () => {
        gfx.clear();

        if (controller.sp) {
            gfx.tile(4, sp.scaled(gfx.width, gfx.height));
        } else {
            gfx.tile(9, sp.scaled(gfx.width, gfx.height));
        }

        window.requestAnimationFrame(paint);
    };

    paint();
};

export const keyboard = () => {
    const KEY_UP = 38;
    const KEY_DN = 40;
    const KEY_LF = 37;
    const KEY_RT = 39;
    const KEY_SP = 32;

    const controller = new Controller();

    window.addEventListener('keydown', (e) => {
        switch (e.keyCode) {
            case KEY_UP: controller.up = true; break;
            case KEY_DN: controller.dn = true; break;
            case KEY_LF: controller.lf = true; break;
            case KEY_RT: controller.rt = true; break;
            case KEY_SP: controller.sp = true; break;
        }
    });

    window.addEventListener('keyup', (e) => {
        switch (e.keyCode) {
            case KEY_UP: controller.up = false; break;
            case KEY_DN: controller.dn = false; break;
            case KEY_LF: controller.lf = false; break;
            case KEY_RT: controller.rt = false; break;
            case KEY_SP: controller.sp = false; break;
        }
    });

    return controller;
};

export const onscreen = (joypadId, buttonId, device) => {
    const controller = new Controller();
    joypad(controller, joypadId, device);
    button(controller, buttonId, device);
    return new Combined(controller, keyboard());
};
