let bridge = null;

if (window.webkit && window.webkit.messageHandlers) {
    bridge = window.webkit.messageHandlers.bridge;
}

class Device {
    rumble(style) {
        if (bridge) {
            bridge.postMessage({ name: 'rumble', style });
        }
    }
}

export class Rumbler {
    constructor(device, ticker, style, ticks, times) {
        this.device = device;
        this.ticked = ticker(ticks);
        this.style  = style;
        this.times  = times;
        this.done   = false;
    }
    tick() {
        if (this.ticked()) {
            this.device.rumble(this.style);
            if (--this.times == 0) {
                this.done = true;
            }
        }
    }
}

export const device = new Device();
